from flask import Flask , request, render_template
from werkzeug.exceptions import ExpectationFailed

app = Flask(__name__)

@app.route('/')

def Home():
    sort_data = [('P','Inbuild Sort'),('Q','Quick Sort'),('B','Bubble Sort'),('M','Merge Sort'),('S','Selection Sort'),('I','Insertion sort'),('SE','Shell Sort')]
    return render_template('index.html',sort_data=sort_data)

@app.route('/sort',methods=['GET',"POST"])

def sorting():
    if request.method == 'POST':
        data = request.form.get('data')
        used_data = request.form.get('sel')
        try:
            data = list(data)
        except:
            return 'Some Error'
        ### Quck sort
        def quck(lis):
            if len(lis)<1:return lis
            povit = lis[-1]
            return quck([x for x in lis if x < povit])+[x for x in lis if x==povit]+quck([x for x in lis if x>povit])

        #sort_d = Quck(data)
        def selection_sort(num):
            for i in range(len(num)-1):
                val = num.index(min(num[i:]))
                num[i],num[val] = num[val],num[i]
            return (num)
        ####
        def Bubble_sort(num):
            while True:
                found = False
                for i in range(len(num)-1):
                    if num[i] > num[i+1]:
                        num[i], num[i+1] = num[i+1] , num[i]
                        found = True
                if not found:
                    return num
        ###
        def insertion_sort(num):
            for i in range(1,len(num)):
                j = i 
                while num[j-1]>num[j] and j>0:
                    num[j],num[j-1]=num[j-1],num[j]
                    j-=1
            return num
        #####

        def merg(left,right):
            data = []
            i,j = 0 , 0 
            while (i<(len(left))) and (j<(len(right))):
                if left[i] <= right[j]:
                    data.append(left[i])
                    i+=1
                else:
                    data.append(right[j])
                    j+=1
            data += left[i:]
            data+= right[j:]
            return data

        def MAIN(lis):
            if len(lis)<=1:
                return lis
            mid = (len(lis)//2)
            left = MAIN(lis[:mid])
            right = MAIN(lis[mid:])
            return merg(left, right)
        
        def shell(lis):
            gap = len(lis)//2
            while gap>0:
                for i in range(gap,len(lis)):
                    temp = lis[i]
                    j=i 
                    while j>=gap and lis[j-gap]>temp:
                        lis[j] = lis[j-gap]
                        j=j-gap
                    lis[j] = temp
                gap = gap//2 
            return lis
        dic_of_data = {
            'P':sorted(data),
            'Q':quck(data),
            'B':Bubble_sort(data),
            'M':MAIN(data),
            'S':selection_sort(data),
            'I':insertion_sort(data),
            'SE': shell(data)}
        data = dic_of_data[used_data]
        s=''
        for i in data:
            s+=str((i))
        cleen = str(set(s))
        return render_template('nav.html',data=s,cleen_d =(cleen),algo=used_data)

if __name__ == '__main__':
    app.run(debug=True)

