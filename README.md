# Flask sorting algorithms

Using shorting algorithms to sort the user input and return clean data 

## Sorting Algorithms

A Sorting Algorithm is used to rearrange a given array or list elements according to a comparison operator on the elements. The comparison operator is used to decide the new order of element in the respective data structure.

For example: 
_The below list of characters is sorted in increasing order of their ASCII values. That is, the character with lesser ASCII value will be placed first than the character with higher ASCII value_


![alt text](https://lamfo-unb.github.io/img/Sorting-algorithms/Complexity.png)

### List of sorting algorithm used in this web app ..!
1. Quicksort:
> Quicksort is an in-place sorting algorithm. Developed by British computer scientist Tony Hoare in 1959 and published in 1961, it is still a commonly used algorithm for sorting. When implemented well, it can be somewhat faster than merge sort and about two or three times faster than heapsort.
![alt text](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.differencebetween.net%2Fwp-content%2Fuploads%2F2018%2F11%2FDifference-between-Quick-Sort-and-Merge-Sort-.png&f=1&nofb=1)

2. Bubble sort:
> Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps through the list, compares adjacent elements and swaps them if they are in the wrong order. The pass through the list is repeated until the list is sorted.
![alt text](https://www.productplan.com/uploads/bubble-sort-1024x683-2.png)

3. Selection sort:
> In computer science, selection sort is an in-place comparison sorting algorithm. It has an O(n²)
time complexity, which makes it inefficient on large lists, and generally performs worse than the
similar insertion sort.
![alt text](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.WehFdNsZBcE-QQ6e9kjWrwHaIh%26pid%3DApi&f=1)

4. Insertion sort
> Insertion sort is a simple sorting algorithm that works similar to the way you sort playing cards in your hands. The array is virtually split into a sorted and an unsorted part. Values from the unsorted part are picked and placed at the correct position in the sorted part.
![alt text](https://media.geeksforgeeks.org/wp-content/uploads/insertionsort.png)

5. Merge Sort:
> Like QuickSort, Merge Sort is a Divide and Conquer algorithm. It divides the input array into two halves, calls itself for the two halves, and then merges the two sorted halves. The merge() function is used for merging two halves. The merge(arr, l, m, r) is a key process that assumes that arr[l..m] and arr[m+1..r] are sorted and merges the two sorted sub-arrays into one. See the following C implementation for details.
![alt text](https://media.geeksforgeeks.org/wp-content/cdn-uploads/Merge-Sort-Tutorial.png)

6. ShellSort:
> ShellSort is mainly a variation of Insertion Sort. In insertion sort, we move elements only one position ahead. When an element has to be moved far ahead, many movements are involved. The idea of shellSort is to allow exchange of far items. In shellSort, we make the array h-sorted for a large value of h. We keep reducing the value of h until it becomes 1. An array is said to be h-sorted if all sublists of every h’th element is sorted.
![alt text](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.iKoID9cWKul1_uDuwp7LagHaDh%26pid%3DApi&f=1)

## How This Web App Works :/
_Using Post method at route ('/sort') takes user data from text area in html file and convert into a List 
and upply a shorting algo (selected by user) and return a set of sorted data_

### HOME PAGE:/
![alt text](https://xp.io/storage/1APqdsZ3.png)

### USER SELECTION DROPDOWN:/
![alt text](https://xp.io/storage/1APysOqe.png)
### Post method :
![alt text](https://xp.io/storage/1APDQ8HW.png)
### Return Sorted  and clean data:
![alt text](https://xp.io/storage/1APJykgg.png)
![alt text](https://xp.io/storage/1APNddaq.png)












